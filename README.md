# MTLSegNet

The official code implementation of the paper "Area-based breast percentage density estimation in mammograms using weight adaptive multi-task learning".




![screenshot](fig1.png)



## Will update the code repository upon the paper acceptance.

## Web application

MTLSegNet: Area-based breast percentage density estimation in mammograms is deployed on Streamlit, an open-source python library for web application. 

### Instructions for web application usage:
1. Upload mammogram in dicom format or in any imaging format, for instance jpg, tif, and png.
2. Click on estimate density, the trained model will be loading
3. The output of the application is breast density of the uploaded mammogram in continous percenatage scale. In addtion, the application output include the sorce image (uploaded mammogram), breast area segmentation and dense tissues segmentation. 



![screenshot](representative.png)

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

